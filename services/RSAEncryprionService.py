import base64
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

def generate_key_pair():
    key = RSA.generate(4096)
    return key

def encrypt(message, public_key):
    cipher = PKCS1_OAEP.new(public_key)
    encrypted_message = cipher.encrypt(message.encode())
    return base64.b64encode(encrypted_message).decode()

def decrypt(encrypted_message, private_key):
    cipher = PKCS1_OAEP.new(private_key)
    decrypted_message = cipher.decrypt(base64.b64decode(encrypted_message))
    return decrypted_message.decode('UTF-8')

def public_key_to_string(public_key):
    pub_key_str = public_key.export_key().decode('UTF-8')
    lines = pub_key_str.split("\n")[1:-1]
    pub_key_str = ''.join(lines)
    return pub_key_str

def string_to_public_key(public_key_string):
    public_key_string = '-----BEGIN PUBLIC KEY-----\n' + public_key_string + '\n-----END PUBLIC KEY-----'
    return RSA.import_key(public_key_string)

def private_key_to_string(private_key):
    return private_key.export_key().decode('UTF-8')

def string_to_private_key(private_key_string):
    return RSA.import_key(private_key_string)

