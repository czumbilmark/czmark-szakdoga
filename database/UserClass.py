from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    tag = Column(String(100), unique=True)
    pub_key = Column(String(5000))
    token_hash = Column(String(300))

    def __init__(self, name, tag, pub_key, token_hash):
        self.name = name
        self.tag = tag
        self.pub_key = pub_key
        self.token_hash = token_hash

engine = create_engine('mysql+pymysql://root:Cmi2002.09.12@localhost:3306/raven_server')
Base.metadata.create_all(engine)