from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class BufferedMessage(Base):
    __tablename__ = 'buffered_messages'

    id = Column(Integer, primary_key=True)
    from_tag = Column(String(100))
    to_tag = Column(String(100))
    message_string = Column(String(5000))

    def __init__(self, from_tag, to_tag, message_string):
        self.from_tag = from_tag
        self.to_tag = to_tag
        self.message_string = message_string

engine = create_engine('mysql+pymysql://root:Cmi2002.09.12@localhost:3306/raven_server')
Base.metadata.create_all(engine)