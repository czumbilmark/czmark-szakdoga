import socket
import threading
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from services.Base36EncoderService import base36_encode
import os
import hashlib
import secrets

import services.RSAEncryprionService as RSAService
from database.UserClass import User
from database.BufferedMessageClass import BufferedMessage

### V 1.0.0 ###

MAXLEN = 2048
# Threadsafe locks
file_lock = threading.Lock()
client_dict_lock = threading.Lock()

# DB SETUP

engine = create_engine('mysql+pymysql://root:Cmi2002.09.12@localhost:3306/raven_server')
Session = sessionmaker(bind=engine)

# RSA SETUP
server_private_key = None
server_public_key = None

clientDict = {}

def setup_rsa():
    global server_private_key
    global server_public_key
    if os.path.exists('data/rsa_key.pem'):
        with open('data/rsa_key.pem', 'r') as f:
            server_private_key = RSA.import_key(f.read())
    else:
        server_private_key = RSAService.generate_key_pair()
        with open('data/rsa_key.pem', 'w') as f:
            f.write(RSAService.private_key_to_string(server_private_key))

    server_public_key = server_private_key.publickey()


def get_next_tag():
    with file_lock:
        counter = 1
        if os.path.exists('data/counter.data'):
            with open('data/counter.data', 'r') as f:
                counter = int(f.read())
        with open('data/counter.data', 'w') as f:
            f.write(str(counter + 1))
    tag = base36_encode(counter).zfill(6)
    return "#" + tag

def generate_token():
    return secrets.token_hex(16)

def hash_token(token):
    return hashlib.sha256(token.encode('UTF-8')).hexdigest()

def receive_message(cs):
    chunks = []
    disconnect = False
    while True:
        chunk = cs.recv(MAXLEN).decode('UTF-8')
        if not chunk:
            disconnect = True
            break
        chunks.append(chunk)
        if '$$$END$$$' in chunk:
            print("should Be breaking now")
            break

    if disconnect:
        return None
    return ''.join(chunks)

def handle_config(cs, lines):
    global server_public_key
    if lines[1].split(":")[0] != "NAME" or lines[2].split(":")[0] != "PUBKEY":
        return
    name = lines[1].split(":")[1]
    user_pubkey_str = lines[2].split(":")[1]

    print(f'Config from {name}')
    tag = get_next_tag()
    token = str(generate_token())
    hashed_token = hash_token(token)

    print(f'TOKEN: {token}, HASHED TOKEN: {hashed_token}')


    user_pubkey = RSAService.string_to_public_key(user_pubkey_str)
    encrypted_token = RSAService.encrypt(token, user_pubkey)

    pub_key_string = RSAService.public_key_to_string(server_public_key)

    cs.sendall(f"STATUS:0\nTAG:{tag}\nTOKEN:{encrypted_token}\nSPUBK:{pub_key_string}\n$$$END$$$\n".encode('UTF-8'))

    session = Session()
    new_user = User(name, tag, user_pubkey_str, hashed_token)

    session.add(new_user)
    session.commit()
    session.close()


def send_buffered_messages(cs, tag):
    session = Session()
    messages = session.query(BufferedMessage).filter_by(to_tag=tag).all()
    for message in messages:
        cs.sendall(message.message_string.encode('UTF-8'))
        session.delete(message)
        session.commit()
    session.close()

def handle_handshake(cs, lines):
    user_tag = lines[1].split(":")[1]
    encrypted_token = lines[2].split(":")[1]
    token = RSAService.decrypt(encrypted_token, server_private_key)
    session = Session()
    user = session.query(User).filter_by(tag=user_tag).first()
    if user is not None:
        if user.token_hash ==hash_token(token):
            with client_dict_lock:
                clientDict[user_tag] = cs
            print("Succesfull handshake from: " + str(user_tag))

            bufferSenderThread = threading.Thread(target=send_buffered_messages, args=(cs, user_tag))
            bufferSenderThread.start()
            return 0
        else:
            print("Failed handshake from: " + str(user_tag))
            print(user.token_hash + " != " + hash_token(token))
            return 1

def handle_message(cs, msg):
    lines = msg.split("\n")

    fromTag =  RSAService.decrypt(lines[1].split(":")[1], server_private_key)
    
    toTag = RSAService.decrypt(lines[2].split(":")[1], server_private_key)

    print(f"Message from {fromTag} to {toTag}")

    session = Session()
    user = session.query(User).filter_by(tag=toTag).first()
    if user is not None:
        user_pubKey_String = user.pub_key
        user_pubKey = RSAService.string_to_public_key(user_pubKey_String)
        enc_fromTag = RSAService.encrypt(fromTag, user_pubKey)
        enc_toTag = RSAService.encrypt(toTag, user_pubKey)
        lines[1] = f"FROM:{enc_fromTag}"
        lines[2] = f"TO:{enc_toTag}"
        reconstructed_msg = "\n".join(lines)
        print(f"Reconstructed message: \n{reconstructed_msg}")

        #send to proper user
        with client_dict_lock:
            if(toTag in clientDict.keys()):
                clientDict[toTag].sendall(reconstructed_msg.encode('UTF-8'))
            else:
                session = Session()
                new_message = BufferedMessage(fromTag, toTag, reconstructed_msg)
                session.add(new_message)
                session.commit()
                session.close()

def main_communication_line(cs, lines):
    com_line_tag = lines[1].split(":")[1]
    if handle_handshake(cs, lines) == 0:
        while True:
            msg = receive_message(cs)
            print(f"Message incoming on com line: {com_line_tag}")   
            if not msg:
                print(f'Disconnected from comLine: {com_line_tag}')
                keyToDelete = None
                with client_dict_lock:
                    for key in clientDict.keys():
                        if clientDict[key] == cs:
                            keyToDelete = key
                    del clientDict[keyToDelete]
                cs.close()
                break
            lines = msg.split("\n")
            command = lines[0].split(":")[1]
            if command == "MSG":
                handle_message(cs, msg)

def handle_getusr(cs, lines):
    user_tag = lines[1].split(":")[1]
    session = Session()
    user = session.query(User).filter_by(tag=user_tag).first()
    if user is not None:
        cs.sendall(f"STATUS:0\nTAG:{user.tag}\nNAME:{user.name}\nPUBKEY:{user.pub_key}\n$$$END$$$\n".encode('UTF-8'))
    else:
        cs.sendall(f"STATUS:2\n$$$END$$$\n".encode('UTF-8'))
    session.close()

def handle_client(cs,index):
    while True:
        msg = cs.recv(MAXLEN)
        if not msg:
            print('Disconnected from :', index)
            keyToDelete = None
            cs.close()
            break
        lines = msg.decode('UTF-8').split("\n")
        command = lines[0].split(":")[1]

        if command == "CONF" :
            handle_config(cs, lines)
        elif command == "HSK":
            main_communication_line(cs, lines)
            break
        elif command == "GETUSR":
            handle_getusr(cs, lines)


print("\n\nSetting up RSA")
setup_rsa()
print("RSA setup complete")

serverPort = 8080
serverSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
index = 0
print(f'\n---------------------------------------------\nThe server is ready to receive on port {serverPort}\n---------------------------------------------\n\n')
while True:
    connectionSocket, addr = serverSocket.accept()
    print('Connected to :', addr[0], ':', addr[1])
    index += 1
    thread = threading.Thread(target=handle_client, args=(connectionSocket, index))
    thread.start()